// 159735 Assignment 1
// Submitted by: 12165471

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <math.h>
#include <mpi.h>

typedef unsigned long int ulong;

using namespace std;

const int MPI_MASTER = 0;

ulong nPrev = 12345, nNext = 0;
double x, y, currentRandom;

ulong GetM(int power = 32);
ulong GetA(int processCount, ulong a, ulong m);
ulong GetC(int processCount, ulong a, ulong c, ulong m);
ulong CalculateAssociativity(ulong a, ulong b, ulong m);
ulong CalculateDistributivity(ulong a, ulong b, ulong m);
double CalculatePI(double x, double y, ulong partition, int processCount, ulong A, ulong C, ulong m);

int main(int argc, char* argv[])
{
	int processCount;
	ulong processPartition;
	double startTime, endTime;

	// constants
	ulong a = 1664525, c = 1013904223, m = GetM();

	int mpiRank;
	MPI_Status mpiStatus;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	MPI_Comm_size(MPI_COMM_WORLD, &processCount);


	if (mpiRank == MPI_MASTER) {
		double pi = 0, piMaster = 0, piRecv = 0;
		vector<double> piData;
		startTime = MPI_Wtime();

		ulong A = GetA(processCount, a, m);
		ulong C = GetC(processCount, a, c, m);
		processPartition = m / processCount;

		for (int index = 0; index < processCount; ++index) {
			nNext = ((a * nPrev) + c) % m;
			nPrev = nNext;

			currentRandom = (double)nNext / (m - 1);

			if (index == 0) {
				x = currentRandom;
				y = nPrev;
			}
			else {
				MPI_Send(&currentRandom, 1, MPI_DOUBLE, index, 0, MPI_COMM_WORLD);
				MPI_Send(&processPartition, 1, MPI_LONG, index, 0, MPI_COMM_WORLD);
				MPI_Send(&nPrev, 1, MPI_LONG, index, 0, MPI_COMM_WORLD);
				MPI_Send(&A, 1, MPI_LONG, index, 0, MPI_COMM_WORLD);
				MPI_Send(&C, 1, MPI_LONG, index, 0, MPI_COMM_WORLD);
			}
		}

		piMaster = CalculatePI(x, y, processPartition, processCount, A, C, m);

		piData.push_back(piMaster);

		for (int index = 1; index < processCount; index++) {
			MPI_Recv(&piRecv, 1, MPI_DOUBLE, index, 0, MPI_COMM_WORLD, &mpiStatus);
			piData.push_back(piRecv);
		}

		for (int i = 0; i < processCount; i++) {
			pi += piData[i];
		}

		piData.clear();
		endTime = MPI_Wtime();

		fprintf(stdout, "\n\nResults\n");
		fprintf(stdout, "---------------------------------------------\n");
		fprintf(stdout, "PI: %2.11f\n", pi);
		fprintf(stdout, "Processing Time: %f seconds\n", endTime - startTime);
		fprintf(stdout, "Number of processes: %d\n\n", processCount);
	}
	else {
		// calculated variables
		ulong A = 0, C = 0;
		double piSlave = 0;

		MPI_Recv(&x, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &mpiStatus);
		MPI_Recv(&processPartition, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, &mpiStatus);
		MPI_Recv(&nPrev, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, &mpiStatus);
		MPI_Recv(&A, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, &mpiStatus);
		MPI_Recv(&C, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, &mpiStatus);

		piSlave = CalculatePI(x, y, processPartition, processCount, A, C, m);

		MPI_Send(&piSlave, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();
	cin.get();
	return 0;
}

ulong GetM(int power) {
	ulong result = 2;

	for (int i = 1; i < power; i++) {
		result = result * 2;
	}

	return result;
}

ulong GetA(int k, ulong a, ulong m) {
	ulong result = 1;

	for (int i = 0; i < k; ++i) {
		result = (result * a) % m;
	}

	return result;
}

ulong GetC(int processCount, ulong a, ulong c, ulong m) {
	ulong summation = 0;

	for (int i = 0; i < processCount; ++i) {
		ulong temp = 1;
		for (int j = 0; j < i; ++j) {
			temp = (temp * a) % m;
		}

		summation = CalculateDistributivity(summation, temp, m);
		summation = summation % m;
	}

	return CalculateAssociativity(c, summation, m);
}

ulong CalculateDistributivity(ulong a, ulong b, ulong m) {
	return ((a % m) + (b % m)) % m;
}

ulong CalculateAssociativity(ulong a, ulong b, ulong m) {
	return (a * (b % m)) % m;
}

double CalculatePI(double x, double y, ulong partition, int processCount, ulong A, ulong C, ulong m) {
	double dist = 0, totalDist = 0;

	currentRandom = x;
	nPrev = y;

	for (ulong i = 0; i < partition; ++i) {
		dist = sqrt(1 - (currentRandom * currentRandom));
		totalDist += dist;

		currentRandom = (double)nNext / (m - 1);

		nNext = ((A * nPrev) + C) % m;
		nPrev = nNext;
	}

	return (totalDist / m) * 4;
}
