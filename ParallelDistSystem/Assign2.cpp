// 159735 Assignment 2
// Submitted by: 12165471

#include <iostream>
#include <mpi.h>

using namespace std;

const int MPI_MASTER = 0;

int main(int argc, char* argv[]) {

	int processCount;

	int mpiRank;
	MPI_Status mpiStatus;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	MPI_Comm_size(MPI_COMM_WORLD, &processCount);

	if (mpiRank == MPI_MASTER) {

	}
	else {

	}

	MPI_Finalize();
	cin.get();
	return 0;
}
